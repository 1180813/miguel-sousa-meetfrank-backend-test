# README #

## What is this repository for? ##

This repository contains the project developed by Miguel Sousa to answer MeetFrank's back-end test task.

## Set Up ##

* To install dependencies, run:

        npm install

* To run the script, run:

        node index.ts

## Tech ##

* For back-end development was used Node.js and TypeScript

* The database of choice was MongoDB

* The scraping portion of the app was made possible using Cheerio and Axios

* GraphQL was also implemented to display the extracted data

## Usage ##

After starting the application, Spotify's jobs web page is going to be accessed and the needed information is going to be extracted and uploaded to the database automatically and in the background. Before trying to access the data please wait a few moments, due to the amount of listings and the time to extract and upload them.

By accessing the following URL, it is possible to query the database using GraphQL:

    http://localhost:3000/graphql

To expose all objects, use the following query:

    { 
        jobs 
    }


The mutation displays the number of jobs listings in the database, using:

    mutation {
        jobs
    }

## Contact ##

Feel free to e-mail me at `migueldamiaosousa@gmail.com` with any questions or issues about the application.