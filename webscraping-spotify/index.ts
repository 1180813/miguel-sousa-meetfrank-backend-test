require('dotenv/config')
const axios = require("axios");
const express = require("express");
const cheerio = require("cheerio");
const { MongoClient } = require('mongodb');
const { graphqlHTTP } = require("express-graphql");
const { GraphQLJSON } = require('graphql-type-json');
const { makeExecutableSchema } = require("graphql-tools");

const port = process.env.PORT;
const dbUrl = process.env.DB_URL;
const url = process.env.SCRAPE_URL;
const client = new MongoClient(dbUrl);
const app = express.Application = express();

async function main() {
    try {
        await client.connect();
        await getListings(client);
    } finally {
        console.log("Spotify job listings are being imported!");
    }
}

async function getListings(client) {
    const { data } = await axios.get(url);
    const $ = cheerio.load(data);

    $(".postings-wrapper .postings-group .posting").each(async (i, elem) => {
        const job = $(elem).find('a.posting-title');
        const listingUrl = job.attr('href');
        getJobData(listingUrl, client);
    });
}

async function getJobData(listingUrl, client) {
    const { data } = await axios.get(listingUrl);
    const $ = cheerio.load(data);

    $("div.content").each((i, elem) => {
        var title = $(elem).find("div.section-wrapper.accent-section.page-full-width div.section.page-centered.posting-header div.posting-headline h2").text().replace("\n", "");
        var location = $(elem).find("div.section-wrapper.accent-section.page-full-width div.section.page-centered.posting-header div.posting-headline div.posting-categories div.sort-by-time.posting-category.medium-category-label").text().replace(" /", "");
        var applyButtonUrl = $(elem).find("div.section-wrapper.accent-section.page-full-width div.section.page-centered.posting-header div.postings-btn-wrapper a.postings-btn.template-btn-submit.hex-color").attr('href');
        var jobDescription = $(elem).find("div.section-wrapper.page-full-width div.section.page-centered").text().replace("\n", "");

        var listing = { title, location, applyButtonUrl, jobDescription };
        createJobListing(client, listing);
    });
}

async function createJobListing(client, newListings) {
    await client.db("listings").collection("spotifyListings").insertOne(newListings);
}

function sleep(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

var typeDefs = [`
  scalar JSON
  type Query {
    jobs: [JSON]
  }
     
  type Mutation {
    jobs: String
  }
`];

var helloMessage = "Welcome!";

var resolvers = {
    JSON: GraphQLJSON,
    Query: {
        jobs: async () => {
            await client.db("listings").collection("spotifyListings").find({}).toArray(function (err, result) {
                if (err) throw err;
                helloMessage = result;
            });
            await sleep(2000);
            return helloMessage;
        }
    },
    Mutation: {
        jobs: async () => {
            const n = await client.db("listings").collection("spotifyListings").estimatedDocumentCount();
            helloMessage = "There are " + n + " job openings!";
            await sleep(750);
            return helloMessage;
        }
    }
};

app.use(
    '/graphql',
    graphqlHTTP({
        schema: makeExecutableSchema({ typeDefs, resolvers }),
        graphiql: true
    })
);

main().catch(console.error);

app.listen(port, () => console.log(`Node GraphQL API listening on port ${port}!`));